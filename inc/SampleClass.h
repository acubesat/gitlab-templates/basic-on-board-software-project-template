#pragma once

/**
 * This is a doxygen-style comment that should exist in every header file. It has multiple purposes.
 * The author should include in this comment:
 * - The purpose of creating this class
 * - A sample use
 * - Various resources that the coder/reader can study to understand further the reasoning behind the functions
 */
class SampleClass {

};
