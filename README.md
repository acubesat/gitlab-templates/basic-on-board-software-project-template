# Basic On-Board Software Project Template

A template for any simple software project to start from.
It includes libraries as:
- Catch2 for unit testing
- ETL to replace STL in modern C++
Since the libraries are submodules you need to run
  ```
  git submodule update --init
  ```

It also includes a sample class with header and source files, as well as one test case.

To set up GitLab CI, use the YAML file in https://gitlab.com/acubesat/gitlab-templates/pipeline-template/-/blob/main/.gitlab-ci.yml

[NOTE]: Be sure to update `CMakeLists.txt` as well.  